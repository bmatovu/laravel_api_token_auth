## Laravel Api Token Based Authentication ##

### Migration ###

`database/migrations/TIMESTAMP_add_api_token_to_users_table.php`

```
public function up()
{
    Schema::table('users', function (Blueprint $table) {
        $table->string('api_token', 60)->unique()->nullable()->after('remember_token');
    });
}

public function down()
{
    Schema::table('users', function (Blueprint $table) {
        $table->dropColumn('api_token');
    });
}
```

### Model ###

Override Authenticate -> save() method in the User Model `app/User.php`

```
public function save(array $options = [])
{
    if(empty($options['api_token']))
        $this->api_token = str_random(60);

    return parent::save($options);
}
```

### Unauthorized - 401 ###

Edit exception handler to return a json response instead of 
redirecting to login page. 
`app/Exceptions/Handler.php`

```
public function handle($request, Closure $next, $guard = null)
{
    ...

    # If request is from ajax; return json as well
    if ($request->expectsJson() || $request->ajax()) {
        return response()->json(['error' => 'Unauthenticated.'], 401);
    }

    # If authentication fails on api routes; return json instead of redirecting to login
    if(preg_match("/^\/api[\/\w]+/", $request->getPathInfo()))
    {
        return response()->json(['error' => 'Unauthenticated.'], 401);
    }

    return $next($request);
}
```

### Usage ###

#### Globally... ####

`app/Providers/RouteServiceProvider.php`

```
protected function mapApiRoutes()
{
    Route::prefix('api')
        // ->middleware('api')
        ->middleware('auth:api')
        ->namespace($this->namespace)
        ->group(base_path('routes/api.php'));
}
```

#### With routes... ####

`routes/api.php`

```
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
```

### Testing ###

#### Guzzle Http Client ####

```
$guzzle = new GuzzleHttp\Client();

// In body params
$guzzle->request($url, ['api_token' => $api_token]);

// In request headers
$guzzle->request($url, [], ['headers' => ['Authorization' => 'Bearer ' . $api_token]]);

// As auth param
$guzzle->request($url, [], ['auth' => [$username, $api_token]]);
```

#### Postman Client ####

Add a `Authorization` header bearer's token to the headers, like;

`Bearer YOUR_API_TOKEN`