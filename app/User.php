<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    /**
     * Override: Save the model to the database.
     *
     * Set default random api_token
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if(empty($options['api_token']))
            $this->api_token = str_random(60);

        return parent::save($options);
    }

}
